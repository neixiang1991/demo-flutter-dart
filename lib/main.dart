import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:io';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'extensions.dart';

void main() {
  // if (Platform.isAndroid) {
  //   SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
  //       statusBarColor: Colors.transparent, //设置为透明
  //       statusBarIconBrightness: Brightness.dark));
  // }
  runApp(const MaterialApp(home: LoginPage()));
}

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final FocusNode _usernameFocusNode = FocusNode();
  final FocusNode _passwordFocusNode = FocusNode();
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _buttonDisabled = true;
  bool _eyeOpen = false;

  void _updateButtonDisabled() =>
      setState(() => _buttonDisabled = _usernameController.text.isEmpty || _passwordController.text.length < 6);

  @override
  void initState() {
    super.initState();
    _usernameController.text = "";
    _passwordController.text = "";
    _usernameController.addListener(_updateButtonDisabled);
    _passwordController.addListener(_updateButtonDisabled);
  }

  @override
  void dispose() {
    _usernameController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  void _clickLogin() {
    if (_buttonDisabled) return;

    _usernameFocusNode.unfocus();
    _passwordFocusNode.unfocus();

    _request("https://www.fastmock.site/mock/639e18e794db09edd8f347dde6100fa7/demo/login", {
      'username': _usernameController.text,
      'password': _passwordController.text
    }).then((data) => _alert(context, '登录成功')).catchError((error) => _alert(context, error.message));
  }

  Future _request(String url, [Object? params = const {}]) async {
    try {
      var response = await http.post(Uri.parse(url), body: params);

      if (response.statusCode == 200) {
        var data = convert.jsonDecode(response.body);
        if (data['success'] == null || !data['success']) {
          throw Exception(data['desc']);
        }
        return;
      } else {
        throw Exception('请求失败：${response.statusCode}');
      }
    } catch (_) {
      rethrow;
    }
  }

  _alert(BuildContext context, String title) {
    showDialog<String>(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => CupertinoAlertDialog(
        title: Text(
          title,
          style: const TextStyle(fontWeight: FontWeight.normal),
        ),
        actions: [
          CupertinoDialogAction(
              onPressed: () => {Navigator.pop(context)},
              child: const Text(
                "确定",
                style: TextStyle(color: Color(0xFFEE9E00), fontWeight: FontWeight.w600),
              ))
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: SafeArea(
        child: Stack(
          children: [
            const NavLeft(),
            const NavRight(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Logo(),
                TextInput(
                  focusNode: _usernameFocusNode,
                  controller: _usernameController,
                  hintText: "请输入手机号/邮箱",
                  keyboardType: TextInputType.datetime,
                  textInputAction: TextInputAction.next,
                ),
                TextInput(
                  focusNode: _passwordFocusNode,
                  controller: _passwordController,
                  hintText: "请输入登录密码",
                  obscureText: !_eyeOpen,
                  keyboardType: TextInputType.visiblePassword,
                  textInputAction: TextInputAction.done,
                  onSubmitted: _clickLogin,
                  children: [
                    GestureDetector(
                      onTap: () => setState(() => _eyeOpen = !_eyeOpen),
                      child: Container(
                        padding: EdgeInsets.only(left: 6.dp, top: 10.dp, bottom: 10.dp, right: 11.dp),
                        child: Image(
                          image: _eyeOpen
                              ? const AssetImage("images/icon_eye_1.png")
                              : const AssetImage("images/icon_eye_0.png"),
                          width: 15.dp,
                          height: 15.dp,
                        ),
                      ),
                    ),
                    Container(
                      height: 13.dp,
                      width: 1.dp,
                      color: const Color(0xFFE5E5E5),
                    ),
                    SizedBox(width: 11.dp),
                    const Tip("忘记密码"),
                  ],
                ),
                LoginButton(disabled: _buttonDisabled, onPressed: _clickLogin),
                Container(padding: EdgeInsets.only(top: 16.dp, left: 32.dp), child: const Tip("手机验证码登录")),
                const Spacer(),
                Container(
                  padding: EdgeInsets.only(bottom: 94.dp, left: 16.dp, right: 16.dp),
                  child: const Row(
                    children: [
                      Third("微信登录", "images/icon_wechat.png"),
                      Third("支付宝登录", "images/icon_alipay.png"),
                    ],
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}

class NavLeft extends StatelessWidget {
  const NavLeft({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 19.dp,
      left: 16.dp,
      child: Image(
        image: const AssetImage('images/icon_back.png'),
        width: 9.dp,
        height: 15.dp,
      ),
    );
  }
}

class NavRight extends StatelessWidget {
  const NavRight({super.key});

  @override
  Widget build(BuildContext context) {
    return Positioned(
      top: 20.dp,
      right: 16.dp,
      child: Text(
        "预约开通",
        style: TextStyle(color: const Color(0xFF666666), fontSize: 13.dp, fontWeight: FontWeight.w600),
      ),
    );
  }
}

class Logo extends StatelessWidget {
  const Logo({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 109.5.dp, bottom: 45.dp),
      child: Image(
        image: const AssetImage('images/logo_sqb.png'),
        width: 150.dp,
        height: 47.5.dp,
      ),
    );
  }
}

class TextInput extends StatelessWidget {
  final FocusNode focusNode;
  final TextEditingController controller;
  final String? hintText;
  final bool obscureText;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final dynamic onSubmitted;
  final List<Widget> children;

  const TextInput(
      {super.key,
      required this.focusNode,
      required this.controller,
      this.hintText,
      this.obscureText = false,
      this.keyboardType = TextInputType.text,
      this.textInputAction = TextInputAction.done,
      this.onSubmitted = const {},
      this.children = const []});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 49.5.dp,
      margin: EdgeInsets.only(top: 15.dp, left: 32.dp, right: 32.dp),
      decoration: BoxDecoration(border: Border(bottom: BorderSide(color: const Color(0xFFe5E5E5), width: 0.5.dp))),
      child: Row(
        children: [
          Expanded(
              child: TextField(
            decoration: InputDecoration(
                border: InputBorder.none,
                hintText: hintText,
                hintStyle: TextStyle(color: const Color(0xFF999999), fontSize: 14.dp)),
            style: TextStyle(color: Colors.black, fontSize: 18.dp),
            focusNode: focusNode,
            controller: controller,
            obscureText: obscureText,
            keyboardType: keyboardType,
            textInputAction: textInputAction,
            onSubmitted: (_) => onSubmitted(),
          )),
          Visibility(
            visible: controller.text.isNotEmpty,
            child: GestureDetector(
              onTap: () => controller.clear(),
              child: Padding(
                padding: EdgeInsets.only(left: 10.dp, top: 10.dp, bottom: 10.dp),
                child: Image(
                  image: const AssetImage("images/icon_close.png"),
                  width: 15.dp,
                  height: 15.dp,
                ),
              ),
            ),
          ),
          ...children,
        ],
      ),
    );
  }
}

class Tip extends StatelessWidget {
  final String text;

  const Tip(this.text, {super.key});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(color: const Color(0xFF666666), fontSize: 13.dp),
    );
  }
}

class LoginButton extends StatefulWidget {
  final bool disabled;
  final VoidCallback onPressed;

  const LoginButton({super.key, required this.disabled, required this.onPressed});

  @override
  State<LoginButton> createState() => _LoginButtonState();
}

class _LoginButtonState extends State<LoginButton> {
  bool _pressing = false;

  void _update(pressing) => setState(() => _pressing = pressing);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => widget.disabled ? null : widget.onPressed(),
      onTapDown: (_) => _update(true),
      onTapUp: (_) => _update(false),
      onTapCancel: () => _update(false),
      child: Container(
        height: 44.dp,
        alignment: Alignment.center,
        margin: EdgeInsets.only(top: 31.5.dp, left: 32.dp, right: 32.dp),
        decoration: BoxDecoration(
            color: widget.disabled
                ? const Color(0xFFf5e2b8)
                : _pressing
                    ? const Color(0xFFb5802b)
                    : const Color(0xFFEE9E00),
            borderRadius: BorderRadius.circular(22.dp)),
        child: Text(
          "登录",
          style: TextStyle(color: Colors.white, fontSize: 16.dp),
        ),
      ),
    );
  }
}

class Third extends StatelessWidget {
  final String text;
  final String icon;

  const Third(this.text, this.icon, {super.key});

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Column(
      children: [
        Container(
          decoration: BoxDecoration(
              border: Border.all(color: const Color(0xFFe5E5E5), width: 0.5.dp),
              borderRadius: BorderRadius.circular(22.5.dp)),
          child: Image(
            image: AssetImage(icon),
            width: 45.dp,
            height: 45.dp,
          ),
        ),
        SizedBox(height: 6.dp),
        Text(
          text,
          style: TextStyle(color: const Color(0xFF999999), fontSize: 11.dp),
        ),
      ],
    ));
  }
}
