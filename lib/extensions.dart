import 'dart:ui';

extension IntDp on int {
  double get dp => this * window.physicalSize.width / (375 * window.devicePixelRatio);
}

extension DoubleDp on double {
  double get dp => this * window.physicalSize.width / (375 * window.devicePixelRatio);
}
